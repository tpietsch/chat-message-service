#Instructions

java8, tomcat8, maven

installation on mac using HOMEBREW

*should already have java on mac*

brew install tomcat
brew install maven
/usr/local/Cellar/tomcat/[version]/bin/catalina run

http://localhost:8080

you can also follow instructions https://tomcat.apache.org/tomcat-8.0-doc/setup.html

This repository is configured with a git-webhook and will build a new WAR (web application archive) when PUSH-ed to.
The current running build can be viewed here
https://jenkins.treverpietsch.com/blue/organizations/jenkins/service-build
the build will also auto-deploy the latest code to https://chat.treverpietsch.com. You can verify the latest is up n running https://chat.treverpietsch.com/health

To run locally download the project and run 'mvn clean install' (after downloading maven).
A *.war file will be generated. Copy this file into your ${TOMCAT_HOME}/webapps/ROOT.war and restart your instance (or configure war auto-deploy)


##Application Configuration
The appliction has 3 files _src/main/resources_ prefixed with 'app'. This is the place to specify some config properties mainly if you would like this to use an in memoryDB or to use an external SQL db.
You can specify the 'env' using a jvm System property (command line arg -Denv={}) options are dev,prod,ci default is 'dev' is the default env to use. Currently configured for in memoryDB locally and using mysql in 'prod'.

once the build is complete you can download the WAR file 
https://artifactory.treverpietsch.com/libs-release/com/treverpietsch/chat-message-service/1.0.0/chat-message-service-1.0.0.war. Note that the version simply will be overwritten with the latest so the '1.0.0' is always the latest build.

if you are running this service in your own tomcat instance please copy this file into your ${TOMCAT_HOME}/webapps/ROOT.war and restart your instance (or configure war auto-deploy)

Once the service is running you can view some auto-gen docs here that also allow for making some api calls through your browser.
https://chat.treverpietsch.com/swagger-ui.html


I have additionally (attempted) to configure some log monitoring but still have not got the log parse logic perfect. If you would like to check that out you can see
[here](https://kibana.treverpietsch.com/app/kibana). The basic http auth creds are below

* u:myfitnesspal
* p:test


##WARNING
I host on digital ocean and coincidentally they are doing maintenance during this window 
Start: 2017-11-14 02:00 UTC
End: 2017-11-14 04:00 UTC
So im guessing things wont work at those times.

#Decisions

Prefixing this by saying I have never used snapchat so if there is any type of assumable inference I will have missed it.

* Unit tests for the use cases described in the assignment
* Tech choice: Spring/JPA/Hibernate/Java/Servlet/Tomcat. Chose these techs mostly because I know them well and they have also have pretty great benchmarks https://www.techempower.com/benchmarks. I know they are a bit 'dated' but that is also probably what makes them more resilient. 
* Made the /chats/:username throw an error if username was missing because im not sure that would be a valid query
* Have separate POJOs for different Versions of the Chat Object because the JSON serialization/deserialization is a bit different between different endpoints of the service.
* Health check endpoint to verify application is up

#Things to improve
* Models: The models between the endpoints differ a bit. Would maybe do things differently there either by forcing the models to match across or by having some more fancy serialization instead of just different classes....a bit on the fence about any approach so ...?
* validation: Model validation i.e -> expiration time exceeding largest possible INT value, Limit string input size...etc....
* docs: The auto doc is cool but a bit annoying to work with when the models vary. That being said swagger docs are a bit inaccurate mostly just added so I could test easily.
* authentication: I would have liked auth service with some ACL in the service but figured since it wasnt part of the prompt it may have not been worth the effort to integrate and would break any test you would run against my server.
* Datasource: I love mysql however im not sure it scales as easily as cassandra and for the type of data that is being stored here being able to distrubute queries across multiple cassandra nodes would be more scalable than having mysql with slave-master replication. The data in the prompt was using numerical values as primary keys. Im not sure if that was a requirement but its something I would NOT have done otherwise. Having the service or the client generate UUIDs as record ids is a far more scalable mechanism. Would have made the 'cold storage' data store separate from the active storage.
* Logging: Logging is invaluable in my experience. Would have liked to get that a bit more in a working state. 
* REST: im not sure that /chats/:username is a very RESTful endpoint. I would have made username a queryParameter the the /chat/ endpoint instead if it the prompt was modifiable.
* Instant Message: Since this is a chat REST interface I thought it might be nice to make a more 'live' version using websockets. It is Someting I have built in the past and makes for a pretty fun app to show ;)
* Application Container: Maybe would have used a different container to make it easier for others to run without having to 'setup' stuff.
* Add version of app to health check endpoint
* limit client connections to prevent DDOS.(i think i may have already configured my proxy server for this but not sure)
*...the list an prob keep going on and lots of decisions are situational given different constraints
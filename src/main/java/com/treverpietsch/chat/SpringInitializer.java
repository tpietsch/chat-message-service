package com.treverpietsch.chat;

import com.treverpietsch.chat.configuration.ApplicationBeans;
import com.treverpietsch.chat.configuration.ApplicationMVCConfiguration;
import com.treverpietsch.chat.configuration.JPAConfiguration;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

public class SpringInitializer
        extends AbstractAnnotationConfigDispatcherServletInitializer {

    private static Class[] classes = new Class[]{
            ApplicationBeans.class,
            ApplicationMVCConfiguration.class,
            JPAConfiguration.class};

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return classes;
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    public void onStartup(final ServletContext context) throws ServletException {
        super.onStartup(context);
    }
}
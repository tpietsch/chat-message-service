package com.treverpietsch.chat.configuration;


import com.treverpietsch.chat.web.JacksonObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;


@Configuration
@Import(ApplicationConfigurationBeans.class)
public class ApplicationBeans {
    @Bean
    public JacksonObjectMapper objectMapper() {
        return new JacksonObjectMapper();
    }

}

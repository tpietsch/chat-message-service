package com.treverpietsch.chat.configuration;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class ApplicationConfigurationBeans {

    private final static String configFile = "app-" + System.getProperty("env", "dev") + ".properties";

    @Bean
    public static PropertiesConfiguration getConfiguration() throws ConfigurationException {
        FileBasedConfigurationBuilder<PropertiesConfiguration> builder =
                new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
                        .configure(new Parameters().properties()
                                .setFileName(configFile)
                                .setListDelimiterHandler(new DefaultListDelimiterHandler(',')));
        return builder.getConfiguration();
    }

    @Bean
    public static PropertyPlaceholderConfigurer getPlaceholderConfig() {
        PropertyPlaceholderConfigurer conf = new PropertyPlaceholderConfigurer();
        conf.setLocation(new ClassPathResource(configFile));
        return conf;
    }
}

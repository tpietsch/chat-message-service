package com.treverpietsch.chat.configuration;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.util.Properties;

@org.springframework.context.annotation.Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.treverpietsch.chat.database.repositories")
@Import({ApplicationConfigurationBeans.class})
public class JPAConfiguration {

    @Autowired
    Configuration configuration;

    public javax.sql.DataSource dataSource() throws ConfigurationException {
        if (configuration.getBoolean("memory.db", false)) {
            EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
            return builder.setType(EmbeddedDatabaseType.HSQL).build();
        } else {
            PoolProperties properties = new PoolProperties();
            properties.setUsername(configuration.getString("username", "root"));
            properties.setPassword(configuration.getString("password", ""));
            properties.setDriverClassName(configuration.getString("driver", "com.mysql.cj.jdbc.Driver"));
            properties.setUrl(configuration.getString("dburl") + "?useSSL=false&zeroDateTimeBehavior=convertToNull&serverTimezone=UTC");
            properties.setMaxActive(10);
            properties.setMaxIdle(5);
            properties.setMinIdle(5);
            properties.setInitialSize(10);
            properties.setMaxWait(50000);
            properties.setValidationQuery(configuration.getString("validationquery", "SELECT 1"));
            properties.setTestOnBorrow(true);
            properties.setRemoveAbandoned(true);
            properties.setDefaultAutoCommit(false);
            properties.setJdbcInterceptors(
                    "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;" +
                            "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
            return new org.apache.tomcat.jdbc.pool.DataSource(properties);
        }
    }

    @Bean
    public Properties getJpaProps() throws ConfigurationException {
        Properties properties = new Properties();
        properties.setProperty("hibernate.connection.release_mode", "ON_CLOSE");
        if (configuration.getBoolean("create-drop", false)) {
            properties.setProperty("hibernate.hbm2ddl.auto", "create-drop");
            properties.setProperty("spring.jpa.hibernate.ddl-auto", "create-drop");
            properties.setProperty("hibernate.hbm2ddl.import_files_sql_extractor", "org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor");
            properties.setProperty("hibernate.hbm2ddl.import_files", "import.sql");
            properties.setProperty("hibernate.format_sql", "false");
        }
        properties.setProperty("hibernate.dialect", configuration.getString("dialect", "org.hibernate.dialect.MySQL5Dialect"));
        properties.setProperty("hibernate.show_sql", "false");
        return properties;
    }

    @Bean
    public EntityManagerFactory entityManagerFactory() throws ConfigurationException {
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setJpaVendorAdapter(vendorAdapter);
        factory.setJpaProperties(getJpaProps());
        factory.setPackagesToScan(configuration.getStringArray("jpa.models.packages.scan"));
        factory.setDataSource(dataSource());
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public PlatformTransactionManager transactionManager(EntityManagerFactory entityManagerFactory) throws ConfigurationException {
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManagerFactory);
        return jpaTransactionManager;
    }

}

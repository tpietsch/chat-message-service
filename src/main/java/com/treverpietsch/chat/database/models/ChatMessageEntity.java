package com.treverpietsch.chat.database.models;

import java.util.Date;

public interface ChatMessageEntity {
    String getUsername();

    String getText();

    Date getExpiration();
}

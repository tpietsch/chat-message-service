package com.treverpietsch.chat.database.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "chat_instant_message")
@JsonIgnoreProperties(ignoreUnknown = true)
public class HotChatMessageEntity implements ChatMessageEntity {
    private Integer id;
    private String username;
    private String text;
    private Date expiration;
    private long timeout = 60;
    private Boolean isRead = false;

    @Id
    @Column(name = "instant_message_id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "chat_text")
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "expiration_date")
    public Date getExpiration() {
        return new Date((expiration.getTime()/ 1000) * 1000);
    }

    public void setExpiration(Date expiration) {
        this.expiration = expiration;
    }

    @Basic
    @Column(name = "chat_timeout")
    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    @Basic
    @Column(name = "is_read")
    public Boolean getIsRead() {
        return isRead;
    }

    public void setIsRead(Boolean isRead) {
        this.isRead = isRead;
    }
}

package com.treverpietsch.chat.database.repositories;

import com.treverpietsch.chat.database.models.HotChatMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface ChatMessageRepository extends JpaRepository<HotChatMessageEntity, Integer> {

    @Query("select chat from HotChatMessageEntity chat where chat.username = ?1 and chat.isRead=false")
    List<HotChatMessageEntity> findByUsername(String username);
}
package com.treverpietsch.chat.database.repositories;

import com.treverpietsch.chat.database.models.ColdChatMessageEntity;
import com.treverpietsch.chat.database.models.HotChatMessageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatMessageStorageRepository extends JpaRepository<ColdChatMessageEntity, Integer> {
    @Query("select chat from ColdChatMessageEntity chat where chat.username = ?1")
    List<ColdChatMessageEntity> findByUsername(String username);
}
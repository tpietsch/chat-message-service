package com.treverpietsch.chat.web.exception;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionController {

    private static Logger logger = Logger.getLogger(ExceptionController.class);

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ExceptionResponse unexpectedError(Exception ex) {
        logger.error(ex);
        ex.printStackTrace();
        return new ExceptionResponse(ex, HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ExceptionResponse notFound(Exception ex) {
        logger.error(ex);
        ex.printStackTrace();
        return new ExceptionResponse(ex, HttpStatus.NOT_FOUND.value());
    }

    public static class ExceptionResponse {
        public String message;
        public Integer status;

        public ExceptionResponse(Exception ex, int status) {
            this.message = ex.getMessage();
            this.status = status;
        }

        public Integer getStatus() {
            return status;
        }

        public String getMessage() {
            return this.message;
        }
    }
}
package com.treverpietsch.chat.web.rest;

import com.treverpietsch.chat.database.models.HotChatMessageEntity;
import com.treverpietsch.chat.database.repositories.ChatMessageRepository;
import com.treverpietsch.chat.database.repositories.ChatMessageStorageRepository;
import com.treverpietsch.chat.web.rest.models.ChatCreateEntity;
import com.treverpietsch.chat.web.rest.models.ChatCreateResponse;
import com.treverpietsch.chat.web.rest.models.ChatEntity;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Optional;


@RestController
@RequestMapping(value = "/chat", produces = MediaType.APPLICATION_JSON_VALUE)
public class ChatResource {
    private static final String ID = "id";
    private static final String PATH_ID = "/{" + ID + "}";

    @Autowired
    ChatMessageRepository chatMessageRepository;

    @Autowired
    ChatMessageStorageRepository chatMessageStorageRepository;

    @RequestMapping(
            value = PATH_ID,
            method = RequestMethod.GET)
    @Transactional
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ChatEntity.class)
    })
    public ResponseEntity getChatMessage(@PathVariable(ID) String id) {
        Optional<HotChatMessageEntity> chatMessageEntity =  chatMessageRepository.findById(Integer.valueOf(id));
        if(chatMessageEntity.isPresent()){
            return chatMessageEntity
                    .map(ChatEntity::new)
                    .map(x -> ResponseEntity.status(200).body(x))
                    .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND)
                            .build());
        }else{
            return chatMessageStorageRepository.findById(Integer.valueOf(id))
                    .map(ChatEntity::new)
                    .map(x -> ResponseEntity.status(200).body(x))
                    .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND)
                            .build());
        }
    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @Transactional
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Success", response = ChatCreateResponse.class)
    })
    public ResponseEntity saveNewChatMessage(@RequestBody ChatCreateEntity chatCreate) {
        HotChatMessageEntity hotChatMessageEntity = new HotChatMessageEntity();
        hotChatMessageEntity.setTimeout(chatCreate.getTimeout());
        hotChatMessageEntity.setText(chatCreate.getText());
        hotChatMessageEntity.setUsername(chatCreate.getUsername());
        //rounding to nearest second
        hotChatMessageEntity.setExpiration(new Date(System.currentTimeMillis() + (1000 * hotChatMessageEntity.getTimeout())));
        hotChatMessageEntity = chatMessageRepository.save(hotChatMessageEntity);
        return ResponseEntity.status(201).body(new ChatCreateResponse(hotChatMessageEntity));
    }
}


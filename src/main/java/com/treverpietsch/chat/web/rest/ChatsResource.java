package com.treverpietsch.chat.web.rest;

import com.treverpietsch.chat.database.models.ColdChatMessageEntity;
import com.treverpietsch.chat.database.models.HotChatMessageEntity;
import com.treverpietsch.chat.database.repositories.ChatMessageRepository;
import com.treverpietsch.chat.database.repositories.ChatMessageStorageRepository;
import com.treverpietsch.chat.web.rest.models.ChatUserSearchResponse;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.stream.Collectors;


@RestController
@RequestMapping(value = "/chats", produces = MediaType.APPLICATION_JSON_VALUE)
public class ChatsResource {
    private static Logger logger = Logger.getLogger(ChatsResource.class);
    private static final String USERNAME = "username";
    private static final String PATH_USERNAME = "/{" + USERNAME + "}";

    @Autowired
    ChatMessageRepository chatMessageRepository;

    @Autowired
    ChatMessageStorageRepository chatMessageStorageRepository;

    @RequestMapping(
            value = {PATH_USERNAME, "", "/"},
            method = RequestMethod.GET)
    @Transactional
    @ResponseBody
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = ChatUserSearchResponse.class, responseContainer = "List")
    })
    public ResponseEntity getChatsForUserFilteredExpiredMessages(@PathVariable(value = USERNAME, required = false) String username) {
        if (StringUtils.isEmpty(username)){
            logger.error("ChatsResource being queried without username");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

        long currSystemTime = System.currentTimeMillis();
        return ResponseEntity.ok(chatMessageRepository
                .findByUsername(username)
                .stream()
                .map(x -> this.moveToStorage(x, currSystemTime))
                .filter(x -> x.getExpiration().getTime() >= currSystemTime)
                .map(ChatUserSearchResponse::new)
                .collect(Collectors.toList()));
    }

    private HotChatMessageEntity moveToStorage(HotChatMessageEntity hotChatMessageEntity, long currSystemTime){
        if(hotChatMessageEntity.getExpiration().getTime() >= currSystemTime){
            hotChatMessageEntity.setIsRead(true);
        }
        chatMessageRepository.deleteById(hotChatMessageEntity.getId());
        ColdChatMessageEntity storageEntity = new ColdChatMessageEntity(hotChatMessageEntity);
        chatMessageStorageRepository.save(storageEntity);
        return hotChatMessageEntity;
    }
}


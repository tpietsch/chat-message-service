package com.treverpietsch.chat.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping(
        value = "/health",
        produces = MediaType.APPLICATION_JSON_VALUE)
public class HealthCheckResource {

    private static HealthCheckResponse resp = new HealthCheckResponse("ok", HttpStatus.OK.value());

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<HealthCheckResponse> healthCheck() throws IOException {
        return ResponseEntity.ok(resp);
    }

    public static class HealthCheckResponse {
        public String message;
        public int status;

        HealthCheckResponse(String message, int status) {
            this.message = message;
            this.status = status;
        }
    }


}

package com.treverpietsch.chat.web.rest.models;

import com.treverpietsch.chat.database.models.HotChatMessageEntity;

public class ChatCreateResponse {
    private Integer id;

    public ChatCreateResponse(HotChatMessageEntity hotChatMessageEntity) {
        this.id = hotChatMessageEntity.getId();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}

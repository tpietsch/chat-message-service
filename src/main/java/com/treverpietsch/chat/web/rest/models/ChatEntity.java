package com.treverpietsch.chat.web.rest.models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.treverpietsch.chat.database.models.ChatMessageEntity;

import java.util.Date;

public class ChatEntity {
    private String username;
    private String text;
    private Date expiration_date;

    public ChatEntity(ChatMessageEntity x) {
        this.username = x.getUsername();
        this.text = x.getText();
        this.expiration_date = x.getExpiration();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("expiration_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    public Date getExpiration_date() {
        return expiration_date;
    }

    public void setExpiration_date(Date expiration_date) {
        this.expiration_date = expiration_date;
    }
}

package com.treverpietsch.chat.web.rest.models;

import com.treverpietsch.chat.database.models.ColdChatMessageEntity;
import com.treverpietsch.chat.database.models.HotChatMessageEntity;

public class ChatUserSearchResponse {
    private Integer id;
    private String text;

    public ChatUserSearchResponse(HotChatMessageEntity hotChatMessageEntity) {
        this.id = hotChatMessageEntity.getId();
        this.text = hotChatMessageEntity.getText();
    }

    public ChatUserSearchResponse(ColdChatMessageEntity chatMessageEntity) {
        this.id = chatMessageEntity.getId();
        this.text = chatMessageEntity.getText();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

import com.treverpietsch.chat.configuration.ApplicationBeans;
import com.treverpietsch.chat.configuration.ApplicationMVCConfiguration;
import com.treverpietsch.chat.configuration.JPAConfiguration;
import com.treverpietsch.chat.database.models.ColdChatMessageEntity;
import com.treverpietsch.chat.database.models.HotChatMessageEntity;
import com.treverpietsch.chat.database.repositories.ChatMessageRepository;
import com.treverpietsch.chat.database.repositories.ChatMessageStorageRepository;
import com.treverpietsch.chat.web.rest.ChatResource;
import com.treverpietsch.chat.web.rest.ChatsResource;
import com.treverpietsch.chat.web.rest.models.ChatCreateEntity;
import com.treverpietsch.chat.web.rest.models.ChatCreateResponse;
import com.treverpietsch.chat.web.rest.models.ChatEntity;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.context.web.ServletTestExecutionListener;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {
        ApplicationBeans.class,
        ApplicationMVCConfiguration.class,
        JPAConfiguration.class})
@WebAppConfiguration
@TestExecutionListeners(listeners = {ServletTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class})
public class ChatUnitTest {

    private static Logger logger = Logger.getLogger(ChatUnitTest.class);

    @Autowired
    ChatResource chatResource;

    @Autowired
    ChatsResource chatsResource;

    @Autowired
    ChatMessageRepository chatMessageRepository;

    @Autowired
    ChatMessageStorageRepository chatMessageStorageRepository;

    @Test
    public void testMessageNotReturnedIfExpired() throws InterruptedException {
        String username = UUID.randomUUID().toString();
        String text = UUID.randomUUID().toString();
        ChatCreateEntity messageEntity = new ChatCreateEntity();
        messageEntity.setTimeout(5);
        messageEntity.setUsername(username);
        messageEntity.setText(text);
        chatResource.saveNewChatMessage(messageEntity);
        Thread.sleep(5001);
        List<HotChatMessageEntity> messageEntityList =
                (List<HotChatMessageEntity>)chatsResource.getChatsForUserFilteredExpiredMessages(username).getBody();
        assert messageEntityList != null;
    }

    @Test
    public void testMessageNotReturnedAfterReadOnce() {
        String username = UUID.randomUUID().toString();
        String text = UUID.randomUUID().toString();
        ChatCreateEntity messageEntity = new ChatCreateEntity();
        messageEntity.setTimeout(Long.MAX_VALUE);
        messageEntity.setUsername(username);
        messageEntity.setText(text);
        chatResource.saveNewChatMessage(messageEntity);
        chatsResource.getChatsForUserFilteredExpiredMessages(username).getBody();
        List<HotChatMessageEntity> messageEntityList =
                (List<HotChatMessageEntity>)chatsResource.getChatsForUserFilteredExpiredMessages(username).getBody();
        assert messageEntityList != null;
        assert messageEntityList.size() == 0;
    }

    @Test
    public void testExpirationDateSetWithTimeout(){
        long timeout = 10;
        //currTime round to seconds
        long currTime = (System.currentTimeMillis() / 1000) * 1000;
        ChatCreateEntity messageEntity = new ChatCreateEntity();
        messageEntity.setTimeout(timeout);
        ChatCreateResponse rsp = (ChatCreateResponse)chatResource.saveNewChatMessage(messageEntity).getBody();
        assert rsp != null;
        HotChatMessageEntity hotChatMessageEntity = chatMessageRepository.findById(rsp.getId()).get();
        assert hotChatMessageEntity.getExpiration().compareTo(new Date(currTime + timeout * 1000)) == 0;
    }

    @Test
    public void testReadAndExpiredThingsMovedToColdStorage(){
        int total = 5;
        String username = UUID.randomUUID().toString();
        for(int i = 0; i < total;i++){
            ChatCreateEntity messageEntity = new ChatCreateEntity();
            messageEntity.setUsername(username);
            messageEntity.setTimeout(Long.MAX_VALUE);
            chatResource.saveNewChatMessage(messageEntity).getBody();
        }

        assert chatMessageRepository.findByUsername(username).size() == total;
        //READ and Expire all messages for user
        chatsResource.getChatsForUserFilteredExpiredMessages(username);
        assert chatMessageRepository.findByUsername(username).size() == 0;
        assert chatMessageStorageRepository.findByUsername(username).size() == total;
    }

    @Test
    public void testReadAndExpiredThingsMovedToColdStorageButStillRetrievableById(){
        int total = 10;
        String username = UUID.randomUUID().toString();

        createSomeChatMessages(total, username);

        chatsResource.getChatsForUserFilteredExpiredMessages(username).getBody();

        createSomeChatMessages(total, username);
        String uniqueText = UUID.randomUUID().toString();
        ChatCreateEntity messageEntityToTest = new ChatCreateEntity();
        messageEntityToTest.setUsername(username);
        messageEntityToTest.setTimeout(Long.MAX_VALUE);
        messageEntityToTest.setText(uniqueText);
        chatResource.saveNewChatMessage(messageEntityToTest);
        ChatCreateResponse rsp = (ChatCreateResponse)chatResource.saveNewChatMessage(messageEntityToTest).getBody();

        ChatCreateEntity messageEntity = new ChatCreateEntity();
        messageEntity.setUsername(username);
        messageEntity.setTimeout(Long.MAX_VALUE);
        chatResource.saveNewChatMessage(messageEntity);
        chatsResource.getChatsForUserFilteredExpiredMessages(username);

        assert rsp != null;
        assert chatResource.getChatMessage(rsp.getId().toString()).getStatusCode().equals(HttpStatus.OK);
        assert ((ChatEntity)chatResource.getChatMessage(rsp.getId().toString()).getBody()).getText().equals(uniqueText);
    }

    private void createSomeChatMessages(int total, String username) {
        for(int i = 0; i < total;i++){
            ChatCreateEntity messageEntity = new ChatCreateEntity();
            messageEntity.setUsername(username);
            messageEntity.setTimeout(Long.MAX_VALUE);
            chatResource.saveNewChatMessage(messageEntity);
        }
    }

    @Before
    public void setUp() {
        logger.info("Test Setting Up...");
    }

    @After
    public void tearDown() {
        logger.info("Test Cleaning Up...");
    }
}